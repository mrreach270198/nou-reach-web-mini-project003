import React, { Component } from 'react'
import ReactUtterences from 'react-utterances'

const GITHUB_NAMESPACE = "b6pzeusbc54tvhw5jgpyw8pwz2x6gs";
const GITHUB_REPO_COMMENTS = "react-utterances-demo-comment";
 
class MyComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
          type: "pathname",
          issueNumber: -1,
          specificTerm: ""
        };
      }
  render() {
    return (
      <div>
        <ReactUtterences 
        repo={`${GITHUB_NAMESPACE}/${GITHUB_REPO_COMMENTS}`}
        type={this.state.type}
        specificTerm={this.state.specificTerm}
        issueNumber={this.state.issueNumber}
        />
      </div>
    )
  }
}
 
export default MyComponent
import React, { useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { fetchArticleById } from "../redux/Action/ArticleAction";
import MyComponent from "./MyComponent";

export default function ViewArticle() {
  let { id } = useParams();

  const dispatch = useDispatch();
  const state = useSelector((state) => state.ArticleReducer.view);

  useEffect(() => {
    dispatch(fetchArticleById(id));
  }, []);

  return (
    <Container>
      {state ? (
        <>
          <Row>
            <Col md={8}>
              <h1 className="my-3">{state.title}</h1>
              <img
                width="auto"
                height="400"
                alt="..."
                src={
                  state.image
                    ? state.image
                    : "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"
                }
              />
              <p>{state.description}</p>
            </Col>
            <Col className="my-3" md={4}>
              <MyComponent/>
            </Col>
          </Row>
        </>
      ) : (
        <h1>Loading...</h1>
      )}
    </Container>
  );
}

import { combineReducers } from 'redux';
import ArticleReducer from './ArticleReducer';
import AuthorReducer from './AuthorReducer';
import CategoryReducer from './CategoryReducer';

const reducers = combineReducers({
    ArticleReducer,
    AuthorReducer,
    CategoryReducer
});

export default reducers;